#!/usr/bin/env bash

docker run --name car_info_db -p 5432:5432 -e POSTGRES_DB=car_info -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=admin -d postgres
