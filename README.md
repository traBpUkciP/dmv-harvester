# car-info

> Vehicle info scraper from DMV records.

## Usage

- Start DB 

``` sh
    bash scripts/init_db.sh
```

### Open Requests in Postman

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/291b84d7209e572e651c)

### NOTE: Run these 2 commands only once

- Run Get Manufacturers in postman for pages 1-10
- Run Get all Makes in postman

> Database is now filled with makes and manufacturers.

----

## Query the newly filled DB

- Query the DB with Get all Makes (local)
- Query the DB with Get all Manufacturers (local)

## License

Copyright (c) 2017

Licensed under the [MIT license](LICENSE).
