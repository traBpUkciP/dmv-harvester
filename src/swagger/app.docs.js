const swagger = require('feathers-swagger');

const vehiclesSpec = require('./vehicles.docs');
const pkg = require('../../package.json');

module.exports = function () {
  let app = this;

  app.configure(swagger({
    docsPath: '/docs',
    version: pkg.version,
    uiIndex: true,
    info: {
      title: pkg.name,
      description: pkg.description,
      contact: {
        email: pkg.author.email
      },
      version: '2.0',
      license: {
        name: 'MIT'
      }
    },
  }));
  app.configure(vehiclesSpec);
};
