/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
var request = require('request');


var request_options = { method: 'GET',
  url: 'https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes',
  qs: { format: 'json' },
  headers:
   { 'cache-control': 'no-cache' } };


function getAllMakes(options, app)  {
  return new Promise((resolve, reject) => {
    request(request_options, function (error, response, body) {
      if (error) throw new Error(error);
      let data = JSON.parse(body.toString());
      _saveQuery(data, app);
      resolve(data);
    });
  });
}

function _saveQuery(data, app) {
  return new Promise((resolve, reject) => {
    data['Results'].forEach(function(e) {
      app.service('makes').create({
        Make_ID: e.Make_ID,
        Make_Name: e.Make_Name
      }).catch(err => { console.error(err); });
    });
    resolve();
  });
}

class Service {
  constructor (options) {
    this.options = options || {};
  }
  setup(app) {
    this.app = app;
  }

  find (params) {
    return Promise.resolve(
      getAllMakes(params, this.app)
    );
  }

  // get (id, params) {
  //   return Promise.resolve({
  //     id, text: `A new message with ID: ${id}!`
  //   });
  // }

  // create (data, params) {
  //   if (Array.isArray(data)) {
  //     return Promise.all(data.map(current => this.create(current)));
  //   }

  //   return Promise.resolve(data);
  // }

  // update (id, data, params) {
  //   return Promise.resolve(data);
  // }

  // patch (id, data, params) {
  //   return Promise.resolve(data);
  // }

  // remove (id, params) {
  //   return Promise.resolve({ id });
  // }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
