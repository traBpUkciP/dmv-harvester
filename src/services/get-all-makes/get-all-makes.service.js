// Initializes the `getAllMakes` service on path `/GetAllMakes`
const createService = require('./get-all-makes.class.js');
const hooks = require('./get-all-makes.hooks');
const filters = require('./get-all-makes.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    name: 'get-all-makes',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/GetAllMakes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('GetAllMakes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
