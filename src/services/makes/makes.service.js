// Initializes the `makes` service on path `/makes`
const createService = require('feathers-sequelize');
const createModel = require('../../models/makes.model');
const hooks = require('./makes.hooks');
const filters = require('./makes.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'makes',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/makes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('makes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
