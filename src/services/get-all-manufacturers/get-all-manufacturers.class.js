/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
var request = require('request');


var request_options = { method: 'GET',
  url: 'https://vpic.nhtsa.dot.gov/api/vehicles/getallmanufacturers',
  qs: { format: 'json', page: '1' },
  headers:
   { 'cache-control': 'no-cache' } };


// function processFind(options) {
//   let data = getAllManufacturers(options);
//   _saveQuery(data);
//   return data;
// }

function getAllManufacturers(options, app)  {
  return new Promise((resolve, reject) => {
    request_options.qs.page = options.query.page;
    request(request_options, function (error, response, body) {
      if (error) throw new Error(error);
      let data = JSON.parse(body.toString());
      _saveQuery(data, app);
      resolve(data);
    });
  });
}

function _saveQuery(data, app) {
  return new Promise((resolve, reject) => {
    data['Results'].forEach(function(e) {
      app.service('manufacturers').create({
        Country: e.country,
        Mfr_CommonName: e.Mfr_CommonName,
        Mfr_ID: e.Mfr_ID,
        Mfr_Name: e.Mfr_Name,
        VehicleTypes: e.VehicleTypes
      }).catch(err => {
        console.error(err);
        reject(err);
      });
    });
    resolve();
  });
}

class Service {
  constructor (options) {
    this.options = options || {};
  }
  setup(app) {
    this.app = app;
  }

  find (params) {
    return Promise.resolve(
      getAllManufacturers(params, this.app)
    );
  }

  // get (id, params) {
  //   return Promise.resolve({
  //     id, text: `A new message with ID: ${id}!`
  //   });
  // }

  // create (data, params) {
  //   if (Array.isArray(data)) {
  //     return Promise.all(data.map(current => this.create(current)));
  //   }

  //   return Promise.resolve(data);
  // }

  // update (id, data, params) {
  //   return Promise.resolve(data);
  // }

  // patch (id, data, params) {
  //   return Promise.resolve(data);
  // }

  // remove (id, params) {
  //   return Promise.resolve({ id });
  // }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
