// Initializes the `GetAllManufacturers` service on path `/GetAllManufacturers`
const createService = require('./get-all-manufacturers.class.js');
const hooks = require('./get-all-manufacturers.hooks');
const filters = require('./get-all-manufacturers.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    name: 'get-all-manufacturers',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/GetAllManufacturers', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('GetAllManufacturers');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
