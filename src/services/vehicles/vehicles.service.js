// Initializes the `vehicles` service on path `/vehicles`
const createService = require('feathers-sequelize');
const createModel = require('../../models/vehicles.model');
const hooks = require('./vehicles.hooks');
const filters = require('./vehicles.filters');

const docs = require('../../swagger/vehicles.docs');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'vehicles',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  const vehicles = createService(options);
  vehicles.docs = docs;

  app.use('/vehicles', vehicles);

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('vehicles');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
