// Initializes the `models` service on path `/models`
const createService = require('feathers-sequelize');
const createModel = require('../../models/models.model');
const hooks = require('./models.hooks');
const filters = require('./models.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'models',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/models', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('models');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
