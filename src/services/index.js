const vehicles = require('./vehicles/vehicles.service.js');
const getAllManufacturers = require('./get-all-manufacturers/get-all-manufacturers.service.js');
const manufacturers = require('./manufacturers/manufacturers.service.js');
const makes = require('./makes/makes.service.js');
const getAllMakes = require('./get-all-makes/get-all-makes.service.js');
const models = require('./models/models.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(vehicles);
  app.configure(getAllManufacturers);
  app.configure(manufacturers);
  app.configure(makes);
  app.configure(getAllMakes);
  app.configure(models);
};
