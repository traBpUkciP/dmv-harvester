// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const models = sequelizeClient.define('models', {
    Make_ID: {
      type: Sequelize.INTEGER,
      allowNull: false
    },
    Make_Name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    Model_ID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      allowNull: false
    },
    Model_Name: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  models.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return models;
};
