// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const manufacturers = sequelizeClient.define('manufacturers', {
    Mfr_ID: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      allowNull: false
    },
    Mfr_Name: {
      type: Sequelize.STRING
    },
    Mfr_CommonName: {
      type: Sequelize.STRING
    },
    Country: {
      type: Sequelize.STRING
    },
    VehicleTypes: {
      type: Sequelize.ARRAY(Sequelize.JSONB)
    },
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  manufacturers.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };
  return manufacturers;
};
