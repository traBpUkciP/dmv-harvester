const assert = require('assert');
const app = require('../../src/app');

describe('\'getAllMakes\' service', () => {
  it('registered the service', () => {
    const service = app.service('GetAllMakes');

    assert.ok(service, 'Registered the service');
  });
});
