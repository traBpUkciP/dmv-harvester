const assert = require('assert');
const app = require('../../src/app');

describe('\'GetAllManufacturers\' service', () => {
  it('registered the service', () => {
    const service = app.service('GetAllManufacturers');

    assert.ok(service, 'Registered the service');
  });
});
