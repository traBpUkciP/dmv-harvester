const assert = require('assert');
const app = require('../../src/app');

describe('\'makes\' service', () => {
  it('registered the service', () => {
    const service = app.service('makes');

    assert.ok(service, 'Registered the service');
  });
});
